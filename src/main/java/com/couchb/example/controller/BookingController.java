package com.couchb.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.couchb.example.model.Booking;
import com.couchb.example.repositories.BookingRepository;

@RestController
@RequestMapping("/booking")
public class BookingController {
	
	@Autowired
	private BookingRepository bookingRepository;
	
	@GetMapping(value="/all", produces = MediaType.APPLICATION_JSON_VALUE)
	List<Booking> getAllBookings(){
		return (List<Booking>) bookingRepository.findAll();
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	Booking saveBooking(@RequestBody Booking booking) {
		return bookingRepository.save(booking);
	}
}

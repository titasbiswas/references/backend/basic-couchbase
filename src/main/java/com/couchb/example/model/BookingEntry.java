package com.couchb.example.model;

import java.util.ArrayList;
import java.util.List;

import com.couchb.example.model.Booking.BookingBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class BookingEntry {
	private String productCode, quantity, unit, basePrice, sailingBasePrice, totalPrice, bookingEntryType, traveller, gratutityAmount, productBasePrice, ageGroup, inventoryBlock, exchangeRate, bookingEntryStatus, productBaseCurrency, externalSystemReservationID, externalSystemName, balanceDueDate;

}
